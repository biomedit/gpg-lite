#!/bin/bash

podman run --rm -it \
       -v ./:/gpg-lite:ro \
       dccch/gpg-lite-test-container \
       /bin/bash -c 'cd /gpg-lite/; exec "${SHELL:-sh}" /gpg-lite/integration_test/test.sh'
